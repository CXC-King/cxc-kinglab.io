---
title: markdown语法
date: 2024-02-22 14:06:24
tags:
---

## 1. 基础语法 
<a id="section1"></a>
### 1.1 #号个数是标题级别
<br><br><br>
### 1.2 段落语法，用空白行隔开<br>

这是第一段落，简单书写几行字

这是第二段落简单隔开
<br><br><br>

### 1.3 两对星号表示粗体，一对星号表示斜体，三对星号表示粗体又斜体内容

**粗体内容**和正常内容

*斜体内容*

***粗体又斜体***

<br><br><br>

### 1.4 块引用使用>符号,其中可以互相嵌套

> - 第一级引用
>
>
>> - 第二级引用
>
> - 也是**第一级**

1. 第一
2. 第二
5. 第三

- 第一
- 第二
- 第三

<br><br><br>

### 1.5 嵌套中引用块和段落
*   This is the first list item.
*   Here's the second list item.

    > A blockquote would look great below the second list item.

*   And here's the third list item.

**引用段落**

*   This is the first list item.
*   Here's the second list item.

    I need to add another paragraph below the second list item.

*   And here's the third list item.

**代码块**

1.  Open the file.
2.  Find the following code block on line 21:

        <html>
          <head>
            <title>Test</title>
          </head>

3.  Update the title to match the name of your website.

**图片**  
 ![Tux, the Linux mascot](/assets/images/tux.png)

 `` `code` use this``
 At the command prompt, type `nano`.

<br><br><br>

### 1.6 分割线用三个---，链接用[网址名称]（地址 “描述”）

这是一个链接 [Markdown语法](https://markdown.com.cn "最好的markdown教程")。

**尖括号<链接>可以变成可点链接**  
<https://markdown.com.cn>  
<fake@example.com>

强调 链接, 在链接语法前后增加星号。 要将链接表示为代码，请在方括号中添加反引号。  
I love supporting the **[EFF](https://eff.org)**.  
This is the *[Markdown Guide](https://www.markdownguide.org)*.  
See the section on [`code`](#code).
<br><br><br>

### 1.7 图片语法

- 插入图片Markdown语法代码：![图片alt](图片链接 "图片title")。

- 图片跳转链接 [![沙漠中的岩石图片](/assets/img/shiprock.jpg "Shiprock")](https://markdown.com.cn)

反斜杠\ 可以显示回原先字体 \*\*显示粗体** **真粗体**
<br><br><br>

##  2. 扩展语法
<!-- <a id="heading-ids"><a> -->
### 2.1 表格的使用
|表格也是    |   随着列增加长度|
|:---:|:---:|
| one  | tow  |
|   three|**[EFF](https://eff.org)**  |

| Syntax      | Description |
| ----------- | ----------- |
| Header      | Title       |
| Paragraph   | Text        |

<br><br><br>

### 2.2 代码块用```

```c
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```
<br><br><br>

### 2.3 定义列表
First Term  
: This is the definition of the first term.

Second Term  
: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;This is one definition of the second term.  
: This is another definition of the second term.
<br><br><br>

### 2.4 删除线用波浪号~~

~~世界是平坦的。~~ 我们现在知道世界是圆的。
<br><br><br>

### 2.5 任务列表语法（vscode没显示）

- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media
- [ ] test
<br><br><br>

### 2.6 表情符号简介（vscode没显示）

去露营了！ :tent: 很快回来。

真好笑！ :joy:
<br><br><br>

### 2.7 自动网址链接
https://www.baidu.com

[回到顶部](#section1)

[回到标题2](#heading-ids)